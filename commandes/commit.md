# git commit
git commit -m "[MESSAGE]"
## Description
Permet de commiter avec le message `MESSAGE`
## Paramètres
- MESSAGE: Le message de commit
## Exemples d'utilisation
``git commit -m "Premier commit"``