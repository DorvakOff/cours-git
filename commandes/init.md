# git init
git init [PATH]
## Description
Permet d'initialiser un repository git dans le dossier `PATH`
## Paramètres
- PATH: Le dossier de la working copy
## Exemples d'utilisation
``git init ~/git/cours_git``