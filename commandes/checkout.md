# git checkout
git checkout [BRANCH]
## Description
Permet de changer de branch et se positionner sur `BRANCH`
## Paramètres
- BRANCH: La branch sur laquelle se positionner
## Exemples d'utilisation
``git checkout main``