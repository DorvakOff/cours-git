# git merge
git merge [BRANCH]
## Description
Permet de merge une branche `BRANCH` dans celle actuelle
## Paramètres
- BRANCH: La branch sur laquelle on se base
## Exemples d'utilisation
``git merge main``