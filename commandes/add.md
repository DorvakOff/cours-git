# git add
git init [FILE]
## Description
Permet d'ajouter le fichier `FILE` au commit
## Paramètres
- FILE: Le fichier à ajouter
## Exemples d'utilisation
``git add README.md``